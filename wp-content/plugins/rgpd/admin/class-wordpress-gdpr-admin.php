<?php

class WordPress_GDPR_Admin extends WordPress_GDPR
{
    protected $plugin_name;
    protected $version;

    /**
     * Construct GDPR Admin Class
     * @author Agence Point Com
     * @version 1.0.0
     * @since   1.0.0
     * @link    https://agencepoint.com
     * @param   string                         $plugin_name
     * @param   string                         $version
     */
    public function __construct($plugin_name, $version)
    {
        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    /**
     * Load Extensions
     * @author Agence Point Com
     * @version 1.0.0
     * @since   1.0.0
     * @link    https://agencepoint.com
     * @return  boolean
     */
    public function load_extensions()
    {
        if(!is_admin()){
            return false;
        }

        // Load the theme/plugin options
        if (file_exists(plugin_dir_path(dirname(__FILE__)).'admin/options-init.php')) {
            require_once plugin_dir_path(dirname(__FILE__)).'admin/options-init.php';
        }
        return true;
    }

    /**
     * Init
     * @author Agence Point Com
     * @version 1.0.0
     * @since   1.0.0
     * @link    https://agencepoint.com
     * @return  boolean
     */
    public function init()
    {
        global $wordpress_gdpr_options;

        if(!is_admin()){
            $wordpress_gdpr_options = get_option('wordpress_gdpr_options');
        }

        $this->options = $wordpress_gdpr_options;
    }

    public function reorder_menu_items()
    {
        global $submenu;

        if(isset($submenu['wordpress_gdpr_options_options'])) {
            
            $service_categories = $submenu['wordpress_gdpr_options_options'][0];
            // $users = $submenu['wordpress_gdpr_options_options'][1];
            $requests = $submenu['wordpress_gdpr_options_options'][1];
            $services = $submenu['wordpress_gdpr_options_options'][2];
            $settings = $submenu['wordpress_gdpr_options_options'][3];
            $object = $submenu['wordpress_gdpr_options_options'][4];
            $import = $submenu['wordpress_gdpr_options_options'][5];

            $submenu['wordpress_gdpr_options_options'] = array(
                0 => $requests,
                1 => $services,
                2 => $service_categories,
                3 => $settings,
                // 4 => $users,
                4 => $object,
                5 => $import,
            );

        }

        return $submenu;
    }

    public function menu_highlight( $parent_file ){ 

        global $current_screen;

        $taxonomy = $current_screen->taxonomy;
        
        if ( $taxonomy == 'gdpr_service_categories' ) {
            $parent_file = 'wordpress_gdpr_options_options';
        }

        return $parent_file; 
    }   

}