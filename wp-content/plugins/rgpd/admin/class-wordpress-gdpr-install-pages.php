<?php

class WordPress_GDPR_Install_Pages extends WordPress_GDPR
{
    protected $plugin_name;
    protected $version;
    protected $options;

    /**
     * Store Locator Plugin Construct
     * @author Agence Point Com
     * @version 1.0.0
     * @since   1.0.0
     * @link    https://agencepoint.com
     * @param   string                         $plugin_name
     * @param   string                         $version
     */
    public function __construct($plugin_name, $version)
    {
        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }
    /**
     * Init the Public
     * @author Agence Point Com
     * @version 1.0.0
     * @since   1.0.0
     * @link    https://agencepoint.com
     * @return  boolean
     */
    public function init()
    {
        global $wordpress_gdpr_options;

        $this->options = $wordpress_gdpr_options;

        if (!$this->get_option('enable')) {
            return false;
        }

        return true;
    }

    public function check_action()
    {
    	if(!isset($_GET['wordpress_gdpr']) || !is_admin()) {
    		return false;
		}

		if(!isset($_GET['wordpress_gdpr']['install-pages'])) {
			return false;
		}

        $options = get_option('wordpress_gdpr_options');

        // $to = 'pointcom@agencepoint.com';
        // $subject = utf8_decode("max 1");
        // $message = print_r($cart_item_data, TRUE);
        // $headers = 'From: pointcom@agencepoint.com' . "\r\n" .
        // 'Reply-To: pointcom@agencepoint.com' . "\r\n" .
        // 'X-Mailer: PHP/' . phpversion();
        // mail($to, $subject, $message, $headers);

        $nomsociete = $this->get_option('nomsociete');

        $domainName = $this->get_option('domainName');
        if(!empty($domainName)) {
            $domainName = $this->get_option('domainName');
        }else{
            $domainName = "";
        }
        $nomsite = $this->get_option('nomsite');
        if(!empty($nomsite)) {
            $nomsite = $this->get_option('nomsite');
        }else{
            $nomsite = "";
        }
        $nomsociete = $this->get_option('nomsociete');
        if(!empty($nomsociete)) {
            $nomsociete = $this->get_option('nomsociete');
        }else{
            $nomsociete = "";
        }
        $telephone = $this->get_option('telephone');
        if(!empty($telephone)) {
            $telephone = $this->get_option('telephone');
        }else{
            $telephone = "";
        }
        $emailsociete = $this->get_option('emailsociete');
        if(!empty($emailsociete)) {
            $emailsociete = $this->get_option('emailsociete');
        }else{
            $emailsociete = "";
        }
        $capital = $this->get_option('capital');
        if(!empty($capital)) {
            $capital = $this->get_option('capital');
        }else{
            $capital = "";
        }

        $capitalactif = $this->get_option('capitalactif');
        if(!$capitalactif) {
            $montantcapital= '';
        }else{
            $montantcapital = ', au capital de '.$capital;
        }

        $adresse_siege = $this->get_option('adresse_siege');
        if(!empty($adresse_siege)) {
            $adresse_siege = $this->get_option('adresse_siege');
        }else{
            $adresse_siege = "";
        }
        $adresse = $this->get_option('adresse');
        if(!empty($adresse)) {
            $adresse = $this->get_option('adresse');
        }else{
            $adresse = "";
        }
        $nomrepresentant = $this->get_option('nomrepresentant');
        if(!empty($nomrepresentant)) {
            $nomrepresentant = $this->get_option('nomrepresentant');
        }else{
            $nomrepresentant = "";
        }
        $posterepresentant = $this->get_option('posterepresentant');
        if(!empty($posterepresentant)) {
            $posterepresentant = $this->get_option('posterepresentant');
        }else{
            $posterepresentant = "";
        }
        $nomdpo = $this->get_option('nomdpo');
        if(!empty($nomdpo)) {
            $nomdpo = $this->get_option('nomdpo');
        }else{
            $nomdpo = "";
        }
        $teldpo = $this->get_option('teldpo');
        if(!empty($teldpo)) {
            $teldpo = $this->get_option('teldpo');
        }else{
            $teldpo = "";
        }

        $emaildpo = $this->get_option('emaildpo');
        if(!empty($emaildpo)) {
            $emaildpo = $this->get_option('emaildpo');
        }else{
            $emaildpo = "";
        }
        $rcs = $this->get_option('rcs');
        if(!empty($rcs)) {
            $rcs = $this->get_option('rcs').',<br>';
        }else{
            $rcs = "";
        }

        $listecollecte = $this->get_option('listecollecte');
        if(!empty($listecollecte)) {
            $listecollecte = $this->get_option('listecollecte');
        }else{
            $listecollecte = "";
        }


        $siteecommerce = $this->get_option('siteecommerce');
        

        if(!$siteecommerce) {
            $donneesfinancieres = '';
        }else{
            $donneesfinancieres = 'Données financières : dans le cadre du paiement des produits et prestations proposés sur la Plateforme, notre site n’a accès à aucune donnée financière relative à la carte de crédit de l’utilisateur, l’opération de paiement s’effectue directement sur les plateformes de notre banque et / ou de Paypal.';
        }


        if(!$siteecommerce) {
            $transmissiondonneesfinancieres = '';
        }else{
            $transmissiondonneesfinancieres = '
            <li>Quand l’utilisateur utilise les services de paiement, pour la mise en œuvre de ces services, la Plateforme est en relation avec des sociétés bancaires et financières tierces avec lesquelles elle a passé des contrats ;</li>
            ';
        }



        

        


        






        $conservationdonnees = $this->get_option('conservationdonnees');
        if(!empty($conservationdonnees)) {
            $conservationdonnees = $this->get_option('conservationdonnees');
        }else{
            $conservationdonnees = "";
        }

        $urldesabo = $this->get_option('urldesabo');
        if(!empty($urldesabo)) {
            $urldesabo = $this->get_option('urldesabo');
        }else{
            $urldesabo = "";
        }

        $urlpagecontact = $this->get_option('urlpagecontact');
        if(!empty($urlpagecontact)) {
            $urlpagecontact = $this->get_option('urlpagecontact');
        }else{
            $urlpagecontact = "";
        }


        



        $pages = array(
            'privacyCenter' => array (
                'post_title'    => __('Privacy Center', 'wordpress-gdpr'),
                'post_content'  => '[wordpress_gdpr_privacy_center]',
                'post_type' => 'page',
                'post_status'   => 'publish',
                'option'    => 'privacyCenter',
            ),
            'contactDPO'    => array (
                'post_title'    => __('Contact DPO', 'wordpress-gdpr'),
                'post_content'  => '[wordpress_gdpr_contact_dpo]',
                'post_type' => 'page',
                'post_status'   => 'publish',
                'option'    => 'contactDPO',
            ),
            'cookiePolicy'  => array (
                'post_title'    => __('Cookie Policy', 'wordpress-gdpr'),
                'post_content'  => '

<br><br>
<h3><strong>Cookies</strong></h3>

Qu’est-ce qu’un « cookie » ?<br>

Un « Cookie » ou traceur est un fichier électronique déposé sur un terminal (ordinateur, tablette, smartphone,…) et lu par exemple lors de la consultation d’un site internet, de la lecture d’un courrier électronique, de l’installation ou de l’utilisation d’un logiciel ou d’une application mobile et ce quel que soit le type de terminal utilisé (source : <a href="https://www.cnil.fr/fr/cookies-traceurs-que-dit-la-loi" target="_blank" rel="noopener">https://www.cnil.fr/fr/cookies-traceurs-que-dit-la-loi</a> ).<br><br>

En naviguant sur ce site, des « cookies » émanant de la société responsable du site concerné et/ou des sociétés tiers pourront être déposés sur votre terminal.<br><br>

Lors de la première navigation sur ce site, une bannière explicative sur l’utilisation des « cookies » apparaîtra. Dès lors, en poursuivant la navigation, le client et/ou prospect sera réputé informé et avoir accepté l’utilisation desdits « cookies ». Le consentement donné sera valable pour une période de treize (13) mois. L’utilisateur a la possibilité de désactiver les cookies à partir des paramètres de son navigateur.<br><br>

Toutes les informations collectées ne seront utilisées que pour suivre le volume, le type et la configuration du trafic utilisant ce site, pour en développer la conception et l’agencement et à d’autres fins administratives et de planification et plus généralement pour améliorer le service que nous vous offrons.

<br><br>

Les cookies suivants sont présents sur ce site :<br>

<strong>Cookies Google :</strong>
<ul>
<li>Google Analytics : permet de mesurer l’audience du site.</li>
<li>Google tag manager : facilite l’implémentation des tags sur les pages et permet de gérer les balises Google.</li>
<li>Google Adwords Conversion : outil de suivi des campagnes publicitaires adwords.</li>
</ul><br>
<strong>Cookies Facebook :</strong>
<ul>
<li>Facebook social plugins : permet de liker, partager, commenter du contenu avec un compte Facebook.</li>
<li>Facebook Custom Audience : permet d’interagir avec l’audience sur Facebook.</li>
</ul>
<br><br>

La durée de vie de ces cookies est de treize mois.<br><br>

Pour plus d’informations sur l’utilisation, la gestion et la suppression des « cookies », pour tout type de navigateur, nous vous invitons à consulter le lien suivant : <a href="https://www.cnil.fr/fr/cookies-les-outils-pour-les-maitriser" target="_blank" rel="noopener">https://www.cnil.fr/fr/cookies-les-outils-pour-les-maitriser</a>.',
                'post_type' => 'page',
                'post_status'   => 'publish',
                'option'    => 'cookiePolicy',
            ),
            // 'DMCA'  => array (
            //     'post_title'    => __('Contact DMCA', 'wordpress-gdpr'),
            //     'post_content'  => 'Contact DMCA Form needs to be created.',
            //     'post_type' => 'page',
            //     'post_status'   => 'publish',
            //     'option'    => 'DMCA',
            // ),
            'dataRectification'    => array (
                'post_title'    => __('Data Rectification', 'wordpress-gdpr'),
                'post_content'  => '[wordpress_gdpr_data_rectification]',
                'post_type' => 'page',
                'post_status'   => 'publish',
                'option'    => 'dataRectification',
            ),
            // 'disclaimer'    => array (
            //     'post_title'    => __('Disclaimer', 'wordpress-gdpr'),
            //     'post_content'  => 'Put your Disclaimer here.',
            //     'post_type' => 'page',
            //     'post_status'   => 'publish',
            //     'option'    => 'disclaimer',
            // ),
            'forgetMe'  => array (
                'post_title'    => __('Forget Me', 'wordpress-gdpr'),
                'post_content'  => '[wordpress_gdpr_forget_me]',
                'post_type' => 'page',
                'post_status'   => 'publish',
                'option'    => 'forgetMe',
            ),
            'imprint'   => array (
                'post_title'    => __('Mentions légales', 'wordpress-gdpr'),
                'post_content'  => '<h3><strong>Article 1 - Mentions légales</strong></h3>
<strong><u>1.1 Site (ci-après « le site »)</u> :</strong>  '.$nomsite.'

<br><br>

<strong><u>1.2 Éditeur (ci-après « l’éditeur »)</u> :</strong><br>

'.$nomsociete.''.$montantcapital.',<br>

dont le siège social est situé : '.$adresse_siege.',<br>

représentée par '.$nomrepresentant.', en sa qualité de '.$posterepresentant.',<br>

'.$rcs.'

N° de téléphone : '.$telephone.',<br>

Adresse mail : '.$emailsociete.'

<br><br>

<strong><u>1.3 Créateur (ci-après « le créateur »)</u> :</strong>

'.$nomsite.' a été créé par :<br>

Agence Point Com – Agence de communication globale Print &amp; Web<br>
2, rue Salomon de Brosse<br>
66000 PERPIGNAN<br><br>

Tél : 04 68 67 20 17<br>

Email : <a href="mailto:pointcom@agencepoint.com">pointcom@agencepoint.com</a><br>

Site : <a href="https://agencepoint.com" target="_blank" rel="noopener">https://agencepoint.com</a><br>

<br><br>

<strong><u>1.4 Hébergeur (ci-après « l’hébergeur »)</u> :</strong><br>

'.$nomsite.' est hébergé par OVH, dont le siège social est situé 2 RUE KELLERMANN, 59100 ROUBAIX.

<br><br>

<strong><u>1.5 Délégué à la protection des données (DPO):</u></strong><br>

Délégué à la protection des données : '.$nomdpo.', '.$emaildpo.'.<br>

Le DPO est à votre disposition pour toute question relative à la protection de vos données personnelles.

<br><br>
<h3><strong>Article 2 - Accès au site</strong></h3>
L’accès au site et son utilisation sont réservés à un usage strictement personnel. Vous vous engagez à ne pas utiliser ce site et les informations ou données qui y figurent à des fins commerciales, politiques, publicitaires et pour toute forme de sollicitation commerciale et notamment l’envoi de courriers électroniques non sollicités.

<br><br>
<h3><strong>Article 3 - Contenu du site</strong></h3>
Toutes les marques, photographies, textes, commentaires, illustrations, images animées ou non, séquences vidéo, sons, ainsi que toutes les applications informatiques qui pourraient être utilisées pour faire fonctionner ce site et plus généralement tous les éléments reproduits ou utilisés sur le site sont protégés par les lois en vigueur au titre de la propriété intellectuelle.<br><br>

Ils sont la propriété pleine et entière de l’éditeur ou de ses partenaires. Toute reproduction, représentation, utilisation ou adaptation, sous quelque forme que ce soit, de tout ou partie de ces éléments, y compris les applications informatiques, sans l’accord préalable et écrit de l’éditeur, sont strictement interdites. Le fait pour l’éditeur de ne pas engager de procédure dès la prise de connaissance de ces utilisations non autorisées ne vaut pas acceptation desdites utilisations et renonciation aux poursuites.

<br><br>
<h3><strong>Article 4 - Gestion du site</strong></h3>
Pour la bonne gestion du site, l’éditeur pourra à tout moment :<br>
<ul>
suspendre, interrompre ou limiter l’accès à tout ou partie du site, réserver l’accès au site, ou à certaines parties du site, à une catégorie déterminée d’internautes ;

<li>supprimer toute information pouvant en perturber le fonctionnement ou entrant en contravention avec les lois nationales ou internationales, ou avec les règles de la Nétiquette ;</li>

<li>suspendre le site afin de procéder à des mises à jour.</li>
</ul>
<br><br>
<h3><strong>Article 5 - Responsabilités</strong></h3>
La responsabilité de l’éditeur ne peut être engagée en cas de défaillance, panne, difficulté ou interruption de fonctionnement, empêchant l’accès au site ou à une de ses fonctionnalités.<br><br>

Le matériel de connexion au site que vous utilisez est sous votre entière responsabilité. Vous devez prendre toutes les mesures appropriées pour protéger votre matériel et vos propres données notamment d’attaques virales par Internet. Vous êtes par ailleurs seul responsable des sites et données que vous consultez.<br><br>

L’éditeur ne pourra être tenu responsable en cas de poursuites judiciaires à votre encontre :
<ul>
<li>du fait de l’usage du site ou de tout service accessible via Internet ;</li>

<li>du fait du non-respect par vous des présentes conditions générales.</li>
</ul><br>
L’éditeur n’est pas responsable des dommages causés à vous-même, à des tiers et/ou à votre équipement du fait de votre connexion ou de votre utilisation du site et vous renoncez à toute action contre lui de ce fait.<br><br>

Si l’éditeur venait à faire l’objet d’une procédure amiable ou judiciaire à raison de votre utilisation du site, il pourrait se retourner contre vous pour obtenir l’indemnisation de tous les préjudices, sommes, condamnations et frais qui pourraient découler de cette procédure.

<br><br>
<h3><strong>Article 6 - Liens hypertextes</strong></h3>
La mise en place par les utilisateurs de tous liens hypertextes vers tout ou partie du site est autorisée par l’éditeur. Tout lien devra être retiré sur simple demande de l’éditeur.<br><br>

Toute information accessible via un lien vers d’autres sites n’est pas publiée par l’éditeur. L’éditeur ne dispose d’aucun droit sur le contenu présent dans ledit lien.

<br><br>
<h3><strong>Article 7 - Photographies et représentation des produits</strong></h3>
Les photographies de produits, accompagnant leur description, ne sont pas contractuelles et n’engagent pas l’éditeur.

<br><br>
<h3><strong>Article 8 - Loi applicable</strong></h3>
Les présentes conditions d’utilisation du site sont régies par la loi française et soumises à la compétence des tribunaux du siège social de l’éditeur, sous réserve d’une attribution de compétence spécifique découlant d’un texte de loi ou réglementaire particulier.

<br><br>
<h3><strong>Article 9 - Nous contacter</strong></h3>
Pour toute question, information sur les produits présentés sur le site, ou concernant le site lui-même, vous pouvez laisser un message à l’adresse suivante sur notre page de contact : <a href="'.$urlpagecontact.'">'.$urlpagecontact.'</a> ou par email <a href="mailto:'.$emailsociete.'">'.$emailsociete.'</a>',
                'post_type' => 'page',
                'post_status'   => 'publish',
                'option'    => 'imprint',
            ),
            'dataRectification'    => array (
                'post_title'    => __('Data Rectification', 'wordpress-gdpr'),
                'post_content'  => '[wordpress_gdpr_data_rectification]',
                'post_type' => 'page',
                'post_status'   => 'publish',
                'option'    => 'dataRectification',
            ),
            // 'mediaCredits'    => array (
            //     'post_title'    => __('Media Credits', 'wordpress-gdpr'),
            //     'post_content'  => 'Put your Media credits here.',
            //     'post_type' => 'page',
            //     'post_status'   => 'publish',
            //     'option'    => 'mediaCredits',
            // ),
            'requestData'   => array (
                'post_title'    => __('Request Data', 'wordpress-gdpr'),
                'post_content'  => '[wordpress_gdpr_request_data]',
                'post_type' => 'page',
                'post_status'   => 'publish',
                'option'    => 'requestData',
            ),
            'privacyPolicy' => array (
                'post_title'    => __('Privacy Policy', 'wordpress-gdpr'),
                'post_content'  => 'La société '.$nomsociete.', soucieuse des droits des individus, notamment au regard des traitements automatisés, et dans une volonté de transparence avec ses clients, a mis en place une politique reprenant l’ensemble de ces traitements, des finalités poursuivies par ces derniers ainsi que des moyens d’actions à la disposition des individus afin qu’ils puissent au mieux exercer leurs droits.<br><br>

Pour toute information complémentaire sur la protection des données personnelles, nous vous invitons à consulter le site : <a href="https://www.cnil.fr/" target="_blank" rel="noopener">https://www.cnil.fr/</a>

<br><br>
<h3><strong>Article 1 - Collecte et protection des données</strong></h3>
Vos données sont collectées par La société '.$nomsociete.'.<br><br>

Une donnée à caractère personnel désigne toute information concernant une personne physique identifiée ou identifiable (personne concernée); est réputée identifiable une personne qui peut être identifiée, directement ou indirectement, notamment par référence à un nom, un numéro d’identification ou à un ou plusieurs éléments spécifiques, propres à son identité physique, physiologique, génétique, psychique, économique, culturelle ou sociale.<br><br>

Les informations personnelles pouvant être recueillies sur le site sont principalement utilisées par l’éditeur pour la gestion des relations avec vous, et le cas échéant pour le traitement de vos commandes.

<br><br>

Les données personnelles collectées sont les suivantes :<br>

'.$listecollecte.'

'.$donneesfinancieres.'<br><br>

Délégué à la protection des données : '.$nomdpo.', '.$emaildpo.', est à votre disposition pour toute question relative à la protection de vos données personnelles.

<br><br>
<h3><strong>Article 2 - Droit d’accès, de rectification et de déréférencement de vos données</strong></h3>
En application de la réglementation applicable aux données à caractère personnel, les utilisateurs disposent des droits suivants :
<ul>
    <li>Le droit d’accès : ils peuvent exercer leur droit d’accès, pour connaître les données personnelles les concernant, en écrivant à l’adresse électronique suivante. Dans ce cas, avant la mise en œuvre de ce droit, la Plateforme peut demander une preuve de l’identité de l’utilisateur afin d’en vérifier l’exactitude.</li>
    <li>Le droit de rectification : si les données à caractère personnel détenues par la Plateforme sont inexactes, ils peuvent demander la mise à jour des informations.</li>
    <li>Le droit de suppression des données : les utilisateurs peuvent demander la suppression de leurs données à caractère personnel, conformément aux lois applicables en matière de protection des données.</li>
    <li>Le droit à la limitation du traitement : les utilisateurs peuvent demander à la Plateforme de limiter le traitement des données personnelles conformément aux hypothèses prévues par le RGPD.</li>
    <li>Le droit de s’opposer au traitement des données : les utilisateurs peuvent s’opposer à ce que ses données soient traitées conformément aux hypothèses prévues par le RGPD.</li>
    <li>Le droit à la portabilité : ils peuvent réclamer que la Plateforme leur remet les données personnelles qui lui ont fourni pour les transmettre à une nouvelle Plateforme.</li>
</ul><br>
Vous pouvez exercer ce droit en nous contactant, à l’adresse suivante :<br>

'.$nomsociete.'<br>
'.$adresse.'<br>

Ou par email, à l’adresse :<br>

<a href="mailto:'.$emailsociete.'">'.$emailsociete.'</a><br>

Vous pouvez aussi vous adresser à notre délégué à la protection des données : '.$nomdpo.', '.$emaildpo.', qui est à votre disposition pour toute question relative à la protection de vos données personnelles.<br><br>

Toute demande doit être accompagnée de la photocopie d’un titre d’identité en cours de validité signé et faire mention de l’adresse à laquelle l’éditeur pourra contacter le demandeur. La réponse sera adressée dans le mois suivant la réception de la demande. Ce délai d’un mois peut être prolongé de deux mois si la complexité de la demande et/ou le nombre de demandes l’exigent.<br><br>

De plus, et depuis la loi n°2016-1321 du 7 octobre 2016, les personnes qui le souhaitent, ont la possibilité d’organiser le sort de leurs données après leur décès. Pour plus d’information sur le sujet, vous pouvez consulter le site Internet de la CNIL : <a href="https://www.cnil.fr/" target="_blank" rel="noopener">https://www.cnil.fr/</a>.<br><br>

Les utilisateurs peuvent aussi introduire une réclamation auprès de la CNIL sur le site de la CNIL : <a href="https://www.cnil.fr" target="_blank" rel="noopener">https://www.cnil.fr</a>.<br><br>

Nous vous recommandons dans un premier temps de nous contacter avant de déposer une réclamation auprès de la CNIL, nous mettrons en place les dispositions afin de régler votre problème dans les plus brefs délais.

<br><br>
<h3><strong>Article 3 - Utilisation des données</strong></h3>
Les données personnelles collectées auprès des utilisateurs ont pour objectif la mise à disposition des services de la Plateforme, leur amélioration et le maintien d’un environnement sécurisé. La base légale des traitements est l’exécution du contrat entre l’utilisateur et la Plateforme.<br><br>
Plus précisément, les utilisations sont les suivantes :
<ul>
<li>Accès et utilisation de la Plateforme par l’utilisateur ;</li>

<li>Gestion du fonctionnement et optimisation de la Plateforme ;</li>

<li>Mise en œuvre d’une assistance utilisateurs ;</li>

<li>Vérification, identification et authentification des données transmises par l’utilisateur ;</li>

<li>Personnalisation des services en affichant des publicités en fonction de l’historique de navigation de l’utilisateur, selon ses préférences ;</li>

<li>Prévention et détection des fraudes, malwares (malicious softwares ou logiciels malveillants) et gestion des incidents de sécurité ;</li>

<li>Gestion des éventuels litiges avec les utilisateurs ;</li>

<li>Envoi d’informations commerciales et publicitaires, en fonction des préférences de l’utilisateur ;</li>

<li>Organisation des conditions d’utilisation des Services de paiement.</li>
</ul>
<br><br>
<h3><strong>Article 4 - Politique de conservation des données</strong></h3>
La Plateforme conserve vos données pour la durée nécessaire pour vous fournir ses services ou de vous fournir une assistance.<br><br>

'.$conservationdonnees.'

<br><br>
<h3><strong>Article 5 - Partage des données personnelles avec des tiers</strong></h3>
Les données personnelles peuvent être partagées avec des sociétés tierces exclusivement dans l’Union européenne, dans les cas suivants :
<ul>
'.$transmissiondonneesfinancieres.'
<li>Lorsque l’utilisateur publie, dans les zones de commentaires libres de la Plateforme, des informations accessibles au public ;</li>

<li>Quand l’utilisateur autorise le site web d’un tiers à accéder à ses données ;</li>

<li>Quand la Plateforme recourt aux services de prestataires pour fournir l’assistance utilisateurs, la publicité et les services de paiement. Ces prestataires disposent d’un accès limité aux données de l’utilisateur, dans le cadre de l’exécution de ces prestations, et ont une obligation contractuelle de les utiliser en conformité avec les dispositions de la réglementation applicable en matière protection des données à caractère personnel ;</li>

<li>Si la loi l’exige, la Plateforme peut effectuer la transmission de données pour donner suite aux réclamations présentées contre la Plateforme et se conformer aux procédures administratives et judiciaires ;</li>
</ul>
<br><br>
<h3><strong>Article 6 - Offres commerciales</strong></h3>

Vous êtes susceptible de recevoir des offres commerciales de l’éditeur. Si vous ne le souhaitez pas, veuillez nous en informer via le formulaire de contact : <a href="'.$urldesabo.'">'.$urldesabo.'</a>.<br><br>

Si, lors de la consultation du site, vous accédez à des données à caractère personnel, vous devez vous abstenir de toute collecte, de toute utilisation non autorisée et de tout acte pouvant constituer une atteinte à la vie privée ou à la réputation des personnes. L’éditeur décline toute responsabilité à cet égard.<br><br>

Les données sont conservées et utilisées pour une durée conforme à la législation en vigueur.

<br><br>
<h3><strong>Article 7 - Cookies</strong></h3><br>
Qu’est-ce qu’un « cookie » ?<br><br>

Un « Cookie » ou traceur est un fichier électronique déposé sur un terminal (ordinateur, tablette, smartphone,…) et lu par exemple lors de la consultation d’un site internet, de la lecture d’un courrier électronique, de l’installation ou de l’utilisation d’un logiciel ou d’une application mobile et ce quel que soit le type de terminal utilisé (source : <a href="https://www.cnil.fr/fr/cookies-traceurs-que-dit-la-loi" target="_blank" rel="noopener">https://www.cnil.fr/fr/cookies-traceurs-que-dit-la-loi</a> ).<br><br>

En naviguant sur ce site, des « cookies » émanant de la société responsable du site concerné et/ou des sociétés tiers pourront être déposés sur votre terminal.<br><br>

Lors de la première navigation sur ce site, une bannière explicative sur l’utilisation des « cookies » apparaîtra. Dès lors, en poursuivant la navigation, le client et/ou prospect sera réputé informé et avoir accepté l’utilisation desdits « cookies ». Le consentement donné sera valable pour une période de treize (13) mois. L’utilisateur a la possibilité de désactiver les cookies à partir des paramètres de son navigateur.<br><br>

Toutes les informations collectées ne seront utilisées que pour suivre le volume, le type et la configuration du trafic utilisant ce site, pour en développer la conception et l’agencement et à d’autres fins administratives et de planification et plus généralement pour améliorer le service que nous vous offrons.

<br><br>

Les cookies suivants sont présents sur ce site :<br>

<strong>Cookies Google :</strong><br>
<ul>
<li>Google Analytics : permet de mesurer l’audience du site.</li>
<li>Google tag manager : facilite l’implémentation des tags sur les pages et permet de gérer les balises Google.</li>
<li>Google Adwords Conversion : outil de suivi des campagnes publicitaires adwords.</li>
</ul>
<br>
<strong>Cookies Facebook :</strong><br>
<ul>
<li>Facebook social plugins : permet de liker, partager, commenter du contenu avec un compte Facebook.</li>
<li>Facebook Custom Audience : permet d’interagir avec l’audience sur Facebook.</li>
</ul>
<br><br>

La durée de vie de ces cookies est de treize mois.<br><br>

Pour plus d’informations sur l’utilisation, la gestion et la suppression des « cookies », pour tout type de navigateur, nous vous invitons à consulter le lien suivant : <a href="https://www.cnil.fr/fr/cookies-les-outils-pour-les-maitriser" target="_blank" rel="noopener">https://www.cnil.fr/fr/cookies-les-outils-pour-les-maitriser</a>.


<br><br>
<h3><strong>Article 8 - Loi applicable</strong></h3>
Les présentes conditions d’utilisation du site sont régies par la loi française et soumises à la compétence des tribunaux du siège social de l’éditeur, sous réserve d’une attribution de compétence spécifique découlant d’un texte de loi ou réglementaire particulier.

<br><br>
<h3><strong>Article 16 - Nous contacter</strong></h3>
Pour toute question, information sur les produits présentés sur le site, ou concernant le site lui-même, vous pouvez laisser un message à l’adresse suivante sur notre page de contact : <a href="'.$urlpagecontact.'">'.$urlpagecontact.'</a> ou par email <a href="mailto:'.$emailsociete.'">'.$emailsociete.'</a>',
                'post_type' => 'page',
                'post_status'   => 'publish',
                'option'    => 'privacyPolicy',
            ),
            // 'privacySettings'   => array (
            //     'post_title'    => __('Privacy Settings', 'wordpress-gdpr'),
            //     'post_content'  => '[wordpress_gdpr_privacy_settings]',
            //     'post_type' => 'page',
            //     'post_status'   => 'publish',
            //     'option'    => 'privacySettings',
            // ),
//             'termsConditions'   => array (
//                 'post_title'    => __('Terms And Conditions', 'wordpress-gdpr'),
//                 'post_content'  => 'The following is a WooCommerce Terms and Conditions Example.

// Search for and replace the following Terms and Conditions placeholders:

// {your company}
// {your email}
// {your address}
// <h2>Overview</h2>
// This website is operated by {your company}. Throughout the site, the terms “we”, “us” and “our” refer to {your company}. {your company} offers this website, including all information, tools and services available from this site to you, the user, conditioned upon your acceptance of all terms, conditions, policies and notices stated here.

// By visiting our site and/ or purchasing something from us, you engage in our “Service” and agree to be bound by the following terms and conditions (“Terms and Conditions”, “Terms”), including those additional terms and conditions and policies referenced herein and/or available by hyperlink. These Terms and Conditions apply to all users of the site, including without limitation users who are browsers, vendors, customers, merchants, and/ or contributors of content.

// Please read these Terms and Conditions carefully before accessing or using our website. By accessing or using any part of the site, you agree to be bound by these Terms and Conditions. If you do not agree to all the terms and conditions of this agreement, then you may not access the website or use any services. If these Terms and Conditions are considered an offer, acceptance is expressly limited to these Terms and Conditions.

// Any new features or tools which are added to the current store shall also be subject to the Terms and Conditions. You can review the most current version of the Terms and Conditions at any time on this page. We reserve the right to update, change or replace any part of these Terms and Conditions by posting updates and/or changes to our website. It is your responsibility to check this page periodically for changes. Your continued use of or access to the website following the posting of any changes constitutes acceptance of those changes.

// Our store is hosted on Shopify Inc. They provide us with the online e-commerce platform that allows us to sell our products and services to you.
// <h2>Online Store Terms</h2>
// By agreeing to these Terms and Conditions, you represent that you are at least the age of majority in your state or province of residence, or that you are the age of majority in your state or province of residence and you have given us your consent to allow any of your minor dependents to use this site.

// You may not use our products for any illegal or unauthorized purpose nor may you, in the use of the Service, violate any laws in your jurisdiction (including but not limited to copyright laws).

// You must not transmit any worms or viruses or any code of a destructive nature.

// A breach or violation of any of the Terms will result in an immediate termination of your Services.
// <h2>General Conditions</h2>
// We reserve the right to refuse service to anyone for any reason at any time.

// You understand that your content (not including credit card information), may be transferred unencrypted and involve (a) transmissions over various networks; and (b) changes to conform and adapt to technical requirements of connecting networks or devices. Credit card information is always encrypted during transfer over networks.

// You agree not to reproduce, duplicate, copy, sell, resell or exploit any portion of the Service, use of the Service, or access to the Service or any contact on the website through which the service is provided, without express written permission by us.

// The headings used in this agreement are included for convenience only and will not limit or otherwise affect these Terms.
// <h2>Accuracy, Completeness And Timeliness Of Information</h2>
// We are not responsible if information made available on this site is not accurate, complete or current. The material on this site is provided for general information only and should not be relied upon or used as the sole basis for making decisions without consulting primary, more accurate, more complete or more timely sources of information. Any reliance on the material on this site is at your own risk.

// This site may contain certain historical information. Historical information, necessarily, is not current and is provided for your reference only. We reserve the right to modify the contents of this site at any time, but we have no obligation to update any information on our site. You agree that it is your responsibility to monitor changes to our site.
// <h2>Modifications To The Service And Prices</h2>
// Prices for our products are subject to change without notice.

// We reserve the right at any time to modify or discontinue the Service (or any part or content thereof) without notice at any time.

// We shall not be liable to you or to any third-party for any modification, price change, suspension or discontinuance of the Service.
// <h2>Products Or Services</h2>
// Certain products or services may be available exclusively online through the website. These products or services may have limited quantities and are subject to return or exchange only according to our Return Policy.

// We have made every effort to display as accurately as possible the colors and images of our products that appear at the store. We cannot guarantee that your computer monitor’s display of any color will be accurate.

// We reserve the right, but are not obligated, to limit the sales of our products or Services to any person, geographic region or jurisdiction. We may exercise this right on a case-by-case basis. We reserve the right to limit the quantities of any products or services that we offer. All descriptions of products or product pricing are subject to change at anytime without notice, at the sole discretion of us. We reserve the right to discontinue any product at any time. Any offer for any product or service made on this site is void where prohibited.

// We do not warrant that the quality of any products, services, information, or other material purchased or obtained by you will meet your expectations, or that any errors in the Service will be corrected.
// <h2>Accuracy Of Billing And Account Information</h2>
// We reserve the right to refuse any order you place with us. We may, in our sole discretion, limit or cancel quantities purchased per person, per household or per order. These restrictions may include orders placed by or under the same customer account, the same credit card, and/or orders that use the same billing and/or shipping address. In the event that we make a change to or cancel an order, we may attempt to notify you by contacting the e-mail and/or billing address/phone number provided at the time the order was made. We reserve the right to limit or prohibit orders that, in our sole judgment, appear to be placed by dealers, resellers or distributors.

// You agree to provide current, complete and accurate purchase and account information for all purchases made at our store. You agree to promptly update your account and other information, including your email address and credit card numbers and expiration dates, so that we can complete your transactions and contact you as needed.

// For more detail, please review our Returns Policy.
// <h2>Optional Tools</h2>
// We may provide you with access to third-party tools over which we neither monitor nor have any control nor input.

// You acknowledge and agree that we provide access to such tools ”as is” and “as available” without any warranties, representations or conditions of any kind and without any endorsement. We shall have no liability whatsoever arising from or relating to your use of optional third-party tools.

// Any use by you of optional tools offered through the site is entirely at your own risk and discretion and you should ensure that you are familiar with and approve of the terms on which tools are provided by the relevant third-party provider(s).

// We may also, in the future, offer new services and/or features through the website (including, the release of new tools and resources). Such new features and/or services shall also be subject to these Terms and Conditions.
// <h2>Third-Party Links</h2>
// Certain content, products and services available via our Service may include materials from third-parties.

// Third-party links on this site may direct you to third-party websites that are not affiliated with us. We are not responsible for examining or evaluating the content or accuracy and we do not warrant and will not have any liability or responsibility for any third-party materials or websites, or for any other materials, products, or services of third-parties.

// We are not liable for any harm or damages related to the purchase or use of goods, services, resources, content, or any other transactions made in connection with any third-party websites. Please review carefully the third-party’s policies and practices and make sure you understand them before you engage in any transaction. Complaints, claims, concerns, or questions regarding third-party products should be directed to the third-party.
// <h2>User Comments, Feedback And Other Submissions</h2>
// If, at our request, you send certain specific submissions (for example contest entries) or without a request from us you send creative ideas, suggestions, proposals, plans, or other materials, whether online, by email, by postal mail, or otherwise (collectively, ‘comments’), you agree that we may, at any time, without restriction, edit, copy, publish, distribute, translate and otherwise use in any medium any comments that you forward to us. We are and shall be under no obligation (1) to maintain any comments in confidence; (2) to pay compensation for any comments; or (3) to respond to any comments.

// We may, but have no obligation to, monitor, edit or remove content that we determine in our sole discretion are unlawful, offensive, threatening, libelous, defamatory, pornographic, obscene or otherwise objectionable or violates any party’s intellectual property or these Terms and Conditions.

// You agree that your comments will not violate any right of any third-party, including copyright, trademark, privacy, personality or other personal or proprietary right. You further agree that your comments will not contain libelous or otherwise unlawful, abusive or obscene material, or contain any computer virus or other malware that could in any way affect the operation of the Service or any related website. You may not use a false e-mail address, pretend to be someone other than yourself, or otherwise mislead us or third-parties as to the origin of any comments. You are solely responsible for any comments you make and their accuracy. We take no responsibility and assume no liability for any comments posted by you or any third-party.
// <h2>Personal Information</h2>
// Your submission of personal information through the store is governed by our Privacy Policy. To view our Privacy Policy.
// <h2>Errors, Inaccuracies And Omissions</h2>
// Occasionally there may be information on our site or in the Service that contains typographical errors, inaccuracies or omissions that may relate to product descriptions, pricing, promotions, offers, product shipping charges, transit times and availability. We reserve the right to correct any errors, inaccuracies or omissions, and to change or update information or cancel orders if any information in the Service or on any related website is inaccurate at any time without prior notice (including after you have submitted your order).

// We undertake no obligation to update, amend or clarify information in the Service or on any related website, including without limitation, pricing information, except as required by law. No specified update or refresh date applied in the Service or on any related website, should be taken to indicate that all information in the Service or on any related website has been modified or updated.
// <h2>Prohibited Uses</h2>
// In addition to other prohibitions as set forth in the Terms and Conditions, you are prohibited from using the site or its content: (a) for any unlawful purpose; (b) to solicit others to perform or participate in any unlawful acts; (c) to violate any international, federal, provincial or state regulations, rules, laws, or local ordinances; (d) to infringe upon or violate our intellectual property rights or the intellectual property rights of others; (e) to harass, abuse, insult, harm, defame, slander, disparage, intimidate, or discriminate based on gender, sexual orientation, religion, ethnicity, race, age, national origin, or disability; (f) to submit false or misleading information; (g) to upload or transmit viruses or any other type of malicious code that will or may be used in any way that will affect the functionality or operation of the Service or of any related website, other websites, or the Internet; (h) to collect or track the personal information of others; (i) to spam, phish, pharm, pretext, spider, crawl, or scrape; (j) for any obscene or immoral purpose; or (k) to interfere with or circumvent the security features of the Service or any related website, other websites, or the Internet. We reserve the right to terminate your use of the Service or any related website for violating any of the prohibited uses.
// <h2>Disclaimer Of Warranties; Limitation Of Liability</h2>
// We do not guarantee, represent or warrant that your use of our service will be uninterrupted, timely, secure or error-free.

// We do not warrant that the results that may be obtained from the use of the service will be accurate or reliable.

// You agree that from time to time we may remove the service for indefinite periods of time or cancel the service at any time, without notice to you.

// You expressly agree that your use of, or inability to use, the service is at your sole risk. The service and all products and services delivered to you through the service are (except as expressly stated by us) provided ‘as is’ and ‘as available’ for your use, without any representation, warranties or conditions of any kind, either express or implied, including all implied warranties or conditions of merchantability, merchantable quality, fitness for a particular purpose, durability, title, and non-infringement.

// In no case shall {your company}, our directors, officers, employees, affiliates, agents, contractors, interns, suppliers, service providers or licensors be liable for any injury, loss, claim, or any direct, indirect, incidental, punitive, special, or consequential damages of any kind, including, without limitation lost profits, lost revenue, lost savings, loss of data, replacement costs, or any similar damages, whether based in contract, tort (including negligence), strict liability or otherwise, arising from your use of any of the service or any products procured using the service, or for any other claim related in any way to your use of the service or any product, including, but not limited to, any errors or omissions in any content, or any loss or damage of any kind incurred as a result of the use of the service or any content (or product) posted, transmitted, or otherwise made available via the service, even if advised of their possibility. Because some states or jurisdictions do not allow the exclusion or the limitation of liability for consequential or incidental damages, in such states or jurisdictions, our liability shall be limited to the maximum extent permitted by law.
// <h2>Indemnification</h2>
// You agree to indemnify, defend and hold harmless {your company} and our parent, subsidiaries, affiliates, partners, officers, directors, agents, contractors, licensors, service providers, subcontractors, suppliers, interns and employees, harmless from any claim or demand, including reasonable attorneys’ fees, made by any third-party due to or arising out of your breach of these Terms and Conditions or the documents they incorporate by reference, or your violation of any law or the rights of a third-party.
// <h2>Severability</h2>
// In the event that any provision of these Terms and Conditions is determined to be unlawful, void or unenforceable, such provision shall nonetheless be enforceable to the fullest extent permitted by applicable law, and the unenforceable portion shall be deemed to be severed from these Terms and Conditions, such determination shall not affect the validity and enforceability of any other remaining provisions.
// <h2>Termination</h2>
// The obligations and liabilities of the parties incurred prior to the termination date shall survive the termination of this agreement for all purposes.

// These Terms and Conditions are effective unless and until terminated by either you or us. You may terminate these Terms and Conditions at any time by notifying us that you no longer wish to use our Services, or when you cease using our site.

// If in our sole judgment you fail, or we suspect that you have failed, to comply with any term or provision of these Terms and Conditions, we also may terminate this agreement at any time without notice and you will remain liable for all amounts due up to and including the date of termination; and/or accordingly may deny you access to our Services (or any part thereof).
// <h2>Entire Agreement</h2>
// The failure of us to exercise or enforce any right or provision of these Terms and Conditions shall not constitute a waiver of such right or provision.

// These Terms and Conditions and any policies or operating rules posted by us on this site or in respect to The Service constitutes the entire agreement and understanding between you and us and govern your use of the Service, superseding any prior or contemporaneous agreements, communications and proposals, whether oral or written, between you and us (including, but not limited to, any prior versions of the Terms and Conditions).

// Any ambiguities in the interpretation of these Terms and Conditions shall not be construed against the drafting party.
// <h2>Governing Law</h2>
// These Terms and Conditions and any separate agreements whereby we provide you Services shall be governed by and construed in accordance with the laws of {your address}.
// <h2>Changes To Terms and Conditions</h2>
// You can review the most current version of the Terms and Conditions at any time at this page.

// We reserve the right, at our sole discretion, to update, change or replace any part of these Terms and Conditions by posting updates and changes to our website. It is your responsibility to check our website periodically for changes. Your continued use of or access to our website or the Service following the posting of any changes to these Terms and Conditions constitutes acceptance of those changes.
// <h2>Contact Information</h2>
// Questions about the Terms and Conditions should be sent to us at {your-email}.',
//                 'post_type' => 'page',
//                 'post_status'   => 'publish',
//                 'option'    => 'termsConditions',
//             ),
            // 'unsubscribe'   => array (
            //     'post_title'    => __('Unscribe', 'wordpress-gdpr'),
            //     'post_content'  => 'Put your Unsubsribe Form here.',
            //     'post_type' => 'page',
            //     'post_status'   => 'publish',
            //     'option'    => 'unsubscribe',
            // ),
        );

        $first = true;
        $post_parent = 0;

        foreach ($pages as $page) {

            $pageOption = $page['option'] . 'Page';
            $pageEnable = $page['option'] . 'Enable';

            if(!$first) {
                $page['post_parent'] = $post_parent;
            }

            if($first && !empty($options[$pageOption])) {
                $post_parent = $options[$pageOption];
                $first = false;
            }

            if(!empty($options[$pageOption])) {
                continue;
            }

            $page_inserted = wp_insert_post($page);
            if($first && empty($options[$pageOption])) {
                $post_parent = $page_inserted;
                $first = false;
            }

            $options[$pageOption] = $page_inserted;
            $options[$pageEnable] = '1';
        }

        update_option('wordpress_gdpr_options', $options);	
		wp_redirect( get_admin_url() . 'edit.php?post_type=page' );
    }
}