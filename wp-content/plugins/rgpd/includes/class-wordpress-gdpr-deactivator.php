<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://agencepoint.com
 * @since      1.0.0
 *
 * @package    WordPress_GDPR
 * @subpackage WordPress_GDPR/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    WordPress_GDPR
 * @subpackage WordPress_GDPR/includes
 * @author     Agence Point Com
 */
class WordPress_GDPR_Deactivator {

	/**
	 * On Plugin deactivation remove roles
	 * @author Agence Point Com
	 * @version 1.0.0
	 * @since   1.0.0
	 * @link    https://agencepoint.com
	 * @return  [type]                       [description]
	 */
	public static function deactivate() {

	}
}