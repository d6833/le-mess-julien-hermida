<?php

/**
 * Fired during plugin activation
 *
 * @link       https://agencepoint.com
 * @since      1.0.0
 *
 * @package    WordPress_GDPR
 * @subpackage WordPress_GDPR/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    WordPress_GDPR
 * @subpackage WordPress_GDPR/includes
 * @author     Agence Point Com
 */
class WordPress_GDPR_Activator {


    /**
     * On plugin activation -> Assign Caps
     * @author Agence Point Com
     * @version 1.0.0
     * @since   1.0.0
     * @link    https://agencepoint.com
     * @return  [type]                       [description]
     */
	public function activate() 
    {
       
	}
}