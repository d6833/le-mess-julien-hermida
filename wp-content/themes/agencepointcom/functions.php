<?php
/**
 * Agence Point Com functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Agence_Point_Com
 */

if ( ! function_exists( 'agencepointcom_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function agencepointcom_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Agence Point Com, use a find and replace
		 * to change 'agencepointcom' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'agencepointcom', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'agencepointcom' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'agencepointcom_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'agencepointcom_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function agencepointcom_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'agencepointcom_content_width', 640 );
}
add_action( 'after_setup_theme', 'agencepointcom_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function agencepointcom_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'agencepointcom' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'agencepointcom' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'agencepointcom_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function agencepointcom_scripts() {
	wp_enqueue_style( 'agencepointcom-style', get_stylesheet_uri());
//	wp_enqueue_style( 'jqueyr-ui-style', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css' );
//	wp_enqueue_style( 'swipper-style', '//unpkg.com/swiper/swiper-bundle.min.css' );
	// wp_enqueue_style( 'fontAwesome-style', '//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css' );


	wp_enqueue_script( 'fontawesome6', '//kit.fontawesome.com/634de72079.js', array(), '20151215', true );
//	wp_enqueue_script( 'jquery-ui', '//code.jquery.com/ui/1.12.1/jquery-ui.js', array(), '5.0.13', true );
//	wp_enqueue_script( 'swipper', '//unpkg.com/swiper/swiper-bundle.min.js' , array(), '1.0', true);
	wp_enqueue_script( 'agencepointcom-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'agencepointcom-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_enqueue_script( 'agencepointcom-frontend', get_template_directory_uri() . '/js/script-frontend.js', array(), '20151215', true );



    wp_register_style(
        'owlCarousel',
        get_template_directory_uri() . "./OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css",
        array(),
        true
    );

    wp_enqueue_style('owlCarousel');

    wp_register_style(
        'owlCarousel2',
        get_template_directory_uri() . "./OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css",
        array(),
        true
    );
    wp_enqueue_style('owlCarousel2');

    wp_register_script(
        'owlCarouselJs',
        get_template_directory_uri() . "./OwlCarousel2-2.3.4/dist/owl.carousel.min.js",
        true,
        true
    );
    wp_enqueue_script('owlCarouselJs');

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'agencepointcom_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

/*personnalisation*/
show_admin_bar(false);




// Désactiver santé du site
add_action('wp_dashboard_setup', 'themeprefix_remove_dashboard_widget' );
function themeprefix_remove_dashboard_widget() {
	remove_meta_box( 'dashboard_site_health', 'dashboard', 'normal' );
}
add_action( 'admin_menu', 'remove_site_health_menu' );
function remove_site_health_menu(){
	remove_submenu_page( 'tools.php','site-health.php' );
}


// Bouton visiter le site ouverture nouvel onglet
add_action( 'admin_bar_menu', 'vsnt_node', 999 );
function vsnt_node( $wp_admin_bar ) {
	$all_toolbar_nodes = $wp_admin_bar->get_nodes();
	foreach ( $all_toolbar_nodes as $node ) {
		if($node->id == 'site-name' || $node->id == 'view-site')
		{
			$args = $node;
			$args->meta = array('target' => '_blank');
			$wp_admin_bar->add_node( $args );
		}
	}
}

// Désactiver Gutenberg
add_filter('use_block_editor_for_post', '__return_false', 10);
add_filter('use_block_editor_for_post_type', '__return_false', 10);

// Rubrique dans admin pour insérer informations dans top bar
add_action('admin_menu', 'ajout_menu_admin');
function ajout_menu_admin()
{
	add_options_page('Informations haut de page', 'Informations haut de page', 'manage_options', 'functions','infos_top_header');
	add_menu_page(null, 'Informations haut de page','edit_posts','/options-general.php?page=functions', null, '', 32);

}
function infos_top_header()
{
	?>
    <div class="wrap conteneur_infos">
        <h2>Informations à afficher dans la top bar</h2>
        <form method="post" action="options.php">
			<?php wp_nonce_field('update-options') ?>
            <div>Téléphone : </div><div><input style="width:300px;" type="text" name="telephone_topheader" value="<?php echo get_option('telephone_topheader'); ?>" /></div><br>
            <div>Adresse : </div><div><input style="width:300px;" type="text" name="adresse_topheader" value="<?php echo get_option('adresse_topheader'); ?>" /></div><br>
            <div>Horaires : </div><div><input style="width:300px;" type="text" name="horaires_topheader" value="<?php echo get_option('horaires_topheader'); ?>" /></div><br>
            <div>URL page Facebook : </div><div><input style="width:300px;" type="text" name="facebook_topheader" value="<?php echo get_option('facebook_topheader'); ?>" /></div><br>
            <div>URL compte Twitter : </div><div><input style="width:300px;" type="text" name="twitter_topheader" value="<?php echo get_option('twitter_topheader'); ?>" /></div><br>
            <div>URL compte Instagram : </div><div><input style="width:300px;" type="text" name="instagram_topheader" value="<?php echo get_option('instagram_topheader'); ?>" /></div><br>

            <p><input type="submit" name="Submit" value="Enregistrer" /></p>
            <input type="hidden" name="page_options" value="telephone_topheader,adresse_topheader,horaires_topheader,facebook_topheader,twitter_topheader,instagram_topheader" />
            <input type="hidden" name="action" value="update" />
        </form>
    </div>
	<?php
}

// Mise au format téléphone
function sanitize_phone($phone){
	$phone = str_replace(' ','',$phone);
	$phone = substr_replace($phone,'+33',0,1);

	return $phone;
}

// Création menu top Header
function menu_top_header() {
	register_nav_menu('menu-top-header',__( 'Menu en-tête' ));
}
add_action( 'init', 'menu_top_header' );


// Ajout classe role utilisateur dans admin
function add_role_to_body($rolessite) {
	global $current_user;
	$user_role = array_shift($current_user->roles);
	$rolessite .= 'role-'. $user_role;
	return $rolessite;
}
add_filter('admin_body_class', 'add_role_to_body');


// Ajout de css pour espace admin (dissimulation d'éléments non utiles aux users par exemple)
add_action('admin_head', 'my_custom_css');
function my_custom_css()
{
	echo
	'<style>
		.maclasse
		{
		display:none!important;
		}
	</style>';
};

// Sidebar footer
function sidebar_footer() {
	register_sidebar( array(
		'name'          => __( 'Pied de page 1', 'theme_name' ),
		'id'            => 'sidebar-footer1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Pied de page 2', 'theme_name' ),
		'id'            => 'sidebar-footer2',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Pied de page 3', 'theme_name' ),
		'id'            => 'sidebar-footer3',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Pied de page 4', 'theme_name' ),
		'id'            => 'sidebar-footer4',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Pied de page 5', 'theme_name' ),
		'id'            => 'sidebar-footer5',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

}
add_action( 'widgets_init', 'sidebar_footer' );


// Personnalisation page connexion 
function my_login_logo() { ?>
    <style type="text/css">
        .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/logo_apc.png)!important;
            padding-bottom: 0px!important;
            background-size: 300px auto!important;
            min-height: 250px!important;
            width: 300px!important;
        }
        .login-action-login{
            background-color: #313131!important;
        }
        #loginform{
            background-color: rgba(0,0,0,0)!important;
            box-shadow: none!important;
        }
        #loginform label{
            color: #FFF!important;
        }
        .login-action-login #nav a,.login-action-login #backtoblog a{
            color: #FFF!important;
        }
        .login-action-login #nav a:hover,.login-action-login #backtoblog a:hover{
            color: #cccccc!important;
        }
        .submit #wp-submit{
            background-color: #4083a8!important;
            border: 2px solid #FFF!important;
            color: #FFF!important;
            border-radius: 0!important;
        }
        .wp-core-ui .button-primary{
            box-shadow: none!important;
            text-shadow: none!important;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

// Personnalisation url logo page connexion
add_filter( 'login_headerurl', 'custom_loginlogo_url' );
function custom_loginlogo_url($url) {
	return  get_home_url();
}

// Changer title logo hover page login
function put_my_title(){
	$titre_site= get_bloginfo( 'name' );
	return ($titre_site); // changing the title from "Powered by WordPress" to whatever you wish
}
add_filter('login_headertitle', 'put_my_title');


/*
 * Ajout fonction dupliquer posts
 */
function rd_duplicate_post_as_draft(){
	global $wpdb;
	if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
		wp_die('No post to duplicate has been supplied!');
	}

	/*
	 * Nonce verification
	 */
	if ( !isset( $_GET['duplicate_nonce'] ) || !wp_verify_nonce( $_GET['duplicate_nonce'], basename( __FILE__ ) ) )
		return;

	/*
	 * get the original post id
	 */
	$post_id = (isset($_GET['post']) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
	/*
	 * and all the original post data then
	 */
	$post = get_post( $post_id );

	/*
	 * if you don't want current user to be the new post author,
	 * then change next couple of lines to this: $new_post_author = $post->post_author;
	 */
	$current_user = wp_get_current_user();
	$new_post_author = $current_user->ID;

	/*
	 * if post data exists, create the post duplicate
	 */
	if (isset( $post ) && $post != null) {

		/*
		 * new post data array
		 */
		$args = array(
			'comment_status' => $post->comment_status,
			'ping_status'    => $post->ping_status,
			'post_author'    => $new_post_author,
			'post_content'   => $post->post_content,
			'post_excerpt'   => $post->post_excerpt,
			'post_name'      => $post->post_name,
			'post_parent'    => $post->post_parent,
			'post_password'  => $post->post_password,
			'post_status'    => 'draft',
			'post_title'     => $post->post_title,
			'post_type'      => $post->post_type,
			'to_ping'        => $post->to_ping,
			'menu_order'     => $post->menu_order
		);

		/*
		 * insert the post by wp_insert_post() function
		 */
		$new_post_id = wp_insert_post( $args );

		/*
		 * get all current post terms ad set them to the new post draft
		 */
		$taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
		foreach ($taxonomies as $taxonomy) {
			$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
			wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
		}

		/*
		 * duplicate all post meta just in two SQL queries
		 */
		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
		if (count($post_meta_infos)!=0) {
			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
			foreach ($post_meta_infos as $meta_info) {
				$meta_key = $meta_info->meta_key;
				if( $meta_key == '_wp_old_slug' ) continue;
				$meta_value = addslashes($meta_info->meta_value);
				$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
			}
			$sql_query.= implode(" UNION ALL ", $sql_query_sel);
			$wpdb->query($sql_query);
		}


		/*
		 * finally, redirect to the edit post screen for the new draft
		 */
		wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
		exit;
	} else {
		wp_die('Post creation failed, could not find original post: ' . $post_id);
	}
}
add_action( 'admin_action_rd_duplicate_post_as_draft', 'rd_duplicate_post_as_draft' );

/*
 * Add the duplicate link to action list for post_row_actions
 */
function rd_duplicate_post_link( $actions, $post ) {
	if (current_user_can('edit_posts')) {
		$actions['duplicate'] = '<a href="' . wp_nonce_url('admin.php?action=rd_duplicate_post_as_draft&post=' . $post->ID, basename(__FILE__), 'duplicate_nonce' ) . '" title="Dupliquer cette page" rel="permalink">Dupliquer</a>';
	}
	return $actions;
}

add_filter( 'post_row_actions', 'rd_duplicate_post_link', 10, 2 );
add_filter('page_row_actions', 'rd_duplicate_post_link', 10, 2);



// Réactiver justification du texte dans tiny mce
function parallel_admin_head()
{
	add_filter('mce_buttons', 'parallel_mce_buttons', 5);
}
function parallel_mce_buttons($buttons)
{
	$position = array_search('alignright', $buttons);
	if (! is_int($position)) {
		return array_merge($buttons, ['alignjustify']);
	}
	return array_merge(
		array_slice($buttons, 0, $position + 1),
		['alignjustify'],
		array_slice($buttons, $position + 1)
	);
}
add_action('admin_head', 'parallel_admin_head', 5);




// Personnalisation de la structure des permarliens
add_action( 'init', function() {
	global $wp_rewrite;
	$wp_rewrite->set_permalink_structure( '/%postname%/' );
} );

// Retirer commentaires
function desactiver_commentaires() {

	// Turn off comments
	if( '' != get_option( 'default_ping_status' ) ) {
		update_option( 'default_ping_status', '' );
	} // end if

	// Turn off pings
	if( '' != get_option( 'default_comment_status' ) ) {
		update_option( 'default_comment_status', '' );
	} // end if

} // end desactiver_commentaires
add_action( 'after_setup_theme', 'desactiver_commentaires' );



/* @params
 * $img Array retourné par ACF
 * $size Format de l'image
 */
function acf_to_img($img,$size = ''){

	$title = $img['title'];
	$alt = $img['alt'];

	if(!$size){
		$url = $img['url'];
		$width = $img['width'];
		$height = $img['height'];
	}else{
		$url = $img['sizes'][$size];
		$width = $img['sizes'][$size.'_width'];
		$height = $img['sizes'][$size.'_height'];
	}

	$img = "<img width = " . $width ." height = " . $height . " src = " . $url . " title = " . $title . " alt = " . $alt . "/>";

	return $img;
}



// Ajouter étapes panier
function cart_checkout_head(){

	$str = '<div class = "head_cart_checkout">';

	$str .= '<div class = "step-cart step-order">';
	$str .= '<div class = "step-number"> 1 </div>';
	$str .= '<span class = "step-text">Panier</span>';
	$str .= '</div>';

//    $str .= '<div class = sep-step></div>';

	$str .= '<div class = "step-checkout step-order">';
	$str .= '<div class = "step-number"> 2 </div>';
	$str .= '<span class = "step-text">Commander</span>';
	$str .= '</div>';

//  $str .= '<div class = sep-step></div>';

	$str .= '<div class = "step-final step-order">';
	$str .= '<div class = "step-number"> 3 </div>';
	$str .= '<span class = "step-text">Finalisation</span>';
	$str .= '</div>';

	$str .= '</div>';

	echo $str;
}
add_action('woocommerce_before_thankyou', 'cart_checkout_head',99 );
add_action('woocommerce_before_cart','cart_checkout_head' );
add_action('woocommerce_before_checkout_form','cart_checkout_head',9 );


add_action( 'woocommerce_review_order_before_payment', 'wc_privacy_message_below_checkout_button' );

function wc_privacy_message_below_checkout_button() {
	echo '<h3 id="moyen_paiement">Moyens de paiement</h3>';
}

// Ajouter une sidebar qui s'affiche sur le tableau de bord
function sidebar_dashboard() {
	register_sidebar( array(
		'name'          => __( 'Sidebar pour vidéos tutorielles du tableau de bord', 'agencepointcom' ),
		'id'            => 'sidebar-dashboard',
		'description'   => __( 'Ne rien modifier svp', 'agencepointcom' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'sidebar_dashboard' );
function appel_sidebar_sur_dashboard() {
	wp_add_dashboard_widget( 'dashboard_widget', 'Vidéos tutorielles générales', 'appel_sidebar' );
}
add_action( 'wp_dashboard_setup', 'appel_sidebar_sur_dashboard' );
function appel_sidebar( $post, $callback_args ) {
	dynamic_sidebar( 'sidebar-dashboard' );
}


// Désactiver le "charger plus" dans les medias côté admin
class Media_Library_Enable_Infinite_Scrolling {
    public function __construct() {
        $this->add_hooks();
    }
    public function add_hooks() {
        add_filter( 'media_library_infinite_scrolling', '__return_true' );
    }
}
new Media_Library_Enable_Infinite_Scrolling();


// Restaurer l'affichage classique des widgets dans l'admin
function example_theme_support() {
    remove_theme_support( 'widgets-block-editor' );
}
add_action( 'after_setup_theme', 'example_theme_support' );


function acf_to_bouton($texte, $lien, $class){
    $str = "Champ(s) Texte et/ou Lien vide";

    if($texte && $lien){
        $str = '<a class = "' . $class . '" href = "' . $lien . '">' . $texte . '</a>';
    }

    return $str;
}


// Désactiver mises à jour (décommenter les lignes ci-dessous)


// Remove auto p and span wrapper from Contact Form 7 shortcode output
add_filter('wpcf7_form_elements', function($content) {
    $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);

    return $content;
});

add_filter('wpcf7_autop_or_not', 'wpcf7_autop_return_false');
function wpcf7_autop_return_false()
{
    return false;
}



// add_filter( 'auto_update_plugin', '__return_false' );
// add_filter( 'auto_update_theme', '__return_false' );
// function remove_core_updates(){
// global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
// }
// add_filter('pre_site_transient_update_core','remove_core_updates');
// add_filter('pre_site_transient_update_plugins','remove_core_updates');
// add_filter('pre_site_transient_update_themes','remove_core_updates');






