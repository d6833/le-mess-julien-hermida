<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Agence_Point_Com
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Raleway:wght@500;700;900&display=swap" >
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500&display=swap" rel="stylesheet"></head>
<!--<link rel="preconnect" href="https://fonts.googleapis.com">-->
<!--<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>-->
<!--<link href="https://fonts.googleapis.com/css2?family=Sue+Ellen+Francisco&display=swap" rel="stylesheet">-->


<body <?php body_class(); ?>>
<div id="page" class="site">



    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'agencepointcom' ); ?></a>

    <div class="top_header">
        <div class="conteneur_top_header">


            <!--       Menu nav commenter     -->
<!--            --><?php
//            if ( has_nav_menu( 'menu-top-header' ) ) {
//                ?>
<!--                <div class="conteneur_menu_top_header">-->
<!--                    --><?php
//                    wp_nav_menu( array( 'theme_location' => 'menu-top-header' ) );
//                    ?>
<!--                </div>-->
<!--                --><?php
//            } ?>
<!---->
        </div>
    </div>


    <header id="masthead" class="site-header">
        <div class="site-branding">
            <?php
            the_custom_logo();
            if ( is_front_page() && is_home() ) :
                ?>
                <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
            <?php
            else :
                ?>
                <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
            <?php
            endif;
            $agencepointcom_description = get_bloginfo( 'description', 'display' );
            if ( $agencepointcom_description || is_customize_preview() ) :
                ?>
                <p class="site-description"><?php echo $agencepointcom_description; /* WPCS: xss ok. */ ?></p>
            <?php endif; ?>
        </div><!-- .site-branding -->

        <!--       Menu nav commenter     -->
<!--        <nav id="site-navigation" class="main-navigation">-->
<!--            <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">--><?php //esc_html_e( 'Primary Menu', 'agencepointcom' ); ?><!--</button>-->
<!--            --><?php
//            wp_nav_menu( array(
//                'theme_location' => 'menu-1',
//                'menu_id'        => 'primary-menu',
//            ) );
//            ?>
<!--        </nav>-->
        <!-- #site-navigation -->
    </header><!-- #masthead -->

    <div id="content" class="site-content">

        <div class="conteneur_social_top_header">
            <?php
            if(get_option('facebook_topheader')){
                echo '<span id="facebook_topheader"><a href="'.get_option('facebook_topheader').'" target="_blank"><i class="fab fa-facebook-f"></i></a></span>';
            }
            if(get_option('twitter_topheader')){
                echo '<span id="twitter_topheader"><a href="'.get_option('twitter_topheader').'" target="_blank"><i class="fab fa-twitter"></i></a></span>';
            }
            if(get_option('instagram_topheader')){
                echo '<span id="instagram_topheader"><a href="'.get_option('instagram_topheader').'" target="_blank"><i class="fab fa-instagram"></i></a></span>';
            }
            ?>

        </div>

        <div class="honeycomb">

