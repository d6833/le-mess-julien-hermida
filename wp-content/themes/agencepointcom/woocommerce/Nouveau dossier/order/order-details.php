
<?php
/**
 * Order details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

$order = wc_get_order( $order_id ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited

if ( ! $order ) {
	return;
}

$order_items           = $order->get_items( apply_filters( 'woocommerce_purchase_order_item_types', 'line_item' ) );
$show_purchase_note    = $order->has_status( apply_filters( 'woocommerce_purchase_note_order_statuses', array( 'completed', 'processing' ) ) );
$show_customer_details = is_user_logged_in() && $order->get_user_id() === get_current_user_id();
$downloads             = $order->get_downloadable_items();
$show_downloads        = $order->has_downloadable_item() && $order->is_download_permitted();

if ( $show_downloads ) {
	wc_get_template(
		'order/order-downloads.php',
		array(
			'downloads'  => $downloads,
			'show_title' => true,
		)
	);
}
?>
<section class="woocommerce-order-details">
	<?php do_action( 'woocommerce_order_details_before_order_table', $order ); ?>

	<table class="woocommerce-table woocommerce-table--order-details shop_table order_details">
		<thead>
			<tr>
                <th class="product-name"><?php esc_html_e( 'Product', 'woocommerce' ); ?></th>
                <th class="product-quantity"><?php esc_html_e( 'Quantity', 'woocommerce' ); ?></th>
                <th class="product-price"><?php esc_html_e( 'Prix HT', 'woocommerce' ); ?></th>
                <th class="product-price"><?php esc_html_e( 'Livraison HT', 'woocommerce' ); ?></th>
                <th class="product-subtotal"><?php esc_html_e( 'Total HT', 'woocommerce' ); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php
			do_action( 'woocommerce_order_details_before_order_table_items', $order );

			foreach ( $order_items as $item_id => $item ) {
				$product = $item->get_product();
				$image = $product->get_image();
				$name = $item->get_name();
				$qty  = $item->get_quantity();
				$initial_price = $product->get_price();
				$price = $item->get_total();
				$livraison = $price - $initial_price;
				$cat =  get_the_terms( $product->get_id(), 'product_cat' );
				$isContainer = 0;

				foreach($cat as $term){
				    if ($term->term_id == 39 ||$term->term_id == 38 ||$term->term_id == 48){
				        $isContainer = 1;
                    }
                }

                ?>
                <tr>
                    <td class = final-row-product>
                        <div class = product-image-final><?php echo $image ?></div>
                        <div class = final-product-name>
                            <?php echo $name ?>
                            <?php wc_display_item_meta( $item ); ?>
                        </div>
                    </td>
                    <td><?php echo esc_html( $qty ); ?> </td>
                    <td><?php echo wc_price($initial_price) ?></td>
                    <td>
                        <?php if($isContainer == 1){
                            if($livraison != 0){
                                echo wc_price($livraison);
                            }else{
                                echo 'Non incluse';
                            }
                        }else{
                            echo 'Incluse';
                        }
                        ?>
                    </td>
                    <td><?php echo wc_price($price) ?> </td>
                </tr>
                <?php
			}

			do_action( 'woocommerce_order_details_after_order_table_items', $order );
			?>
		</tbody>

	</table>

	<?php do_action( 'woocommerce_order_details_after_order_table', $order ); ?>
</section>