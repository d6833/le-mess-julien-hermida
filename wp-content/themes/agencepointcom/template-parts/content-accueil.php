<?php
/**
 * Template part for displaying page content in template-a-propos.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Agence_Point_Com
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<!--    image d'en tête  -->
<header>
    <div class="accueil-entry-header">
       <div class="acceuil-conteneur-picture">
           <?php
           $image = get_field('image');
           $size = 'full'; // (thumbnail, medium, large, full or custom size)
           if( $image ) {
               echo wp_get_attachment_image( $image, $size );
           } else
               echo 'empty'; ?>
       </div>
    </div>
</header>


    <!--  Titre de millieu de page -->
    <div class="d-fs-20">
        <h2 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h2>
    </div>

<!-- Carousel -->
    <div class="a-propos-container-w">
        <!-- Carousel -->
        <div class="container-carousel">
            <div class="carousel owl-carousel">
                <div class="items item-1">
                    <?php
                    $image = get_field('image3');
                    $size = 'full'; // (thumbnail, medium, large, full or custom size)
                    if( $image ) {
                        echo wp_get_attachment_image( $image, $size );
                    } else
                        echo 'empty'; ?>
                </div>
                <div class="items item-2">
                    <?php
                    $image = get_field('image3');
                    $size = 'full'; // (thumbnail, medium, large, full or custom size)
                    if( $image ) {
                        echo wp_get_attachment_image( $image, $size );
                    } else
                        echo 'empty'; ?>
                </div>
            </div>
        </div>

        <div class="accueil-container-content">
            <?php
            the_content();
            ?>
        </div>
    </div>
    <!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->