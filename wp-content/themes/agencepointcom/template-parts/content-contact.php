<?php
/**
 * Template part for displaying page content in template-contact.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Agence_Point_Com
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="contact-container">
        <header class="contact-entry-header">
            <div class="conteneur_coordonnnees_header">
            <!--        --><?php //the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                <div class="contact-info">
                    <h1 class="contact-entry-title">NOUS CONTACTER</h1>

                    <!--    Editable with the plugin " Informations haut de page " in Wordpress     -->

                    <?php

                    if(get_option('adresse_topheader')){
                        echo '<span class="conteneur_adresse_topheader"><i class="fas fa-map-marker-alt"></i><span id="adresse_topheader">'.get_option('adresse_topheader').'</span></span> <br>';
                    }
                    if(get_option('telephone_topheader')){
                        echo '<span class="conteneur_telephone_topheader"><i class="fas fa-phone-alt"></i><span id="telephone_topheader"><a href = "tel:' . sanitize_phone(get_option('telephone_topheader')) . '">'.get_option('telephone_topheader').'</a></span></span>'; ?>

                        <?php
                    }
                    if(get_option('horaires_topheader')){
                        echo '<span class="conteneur_horaires_topheader desktop"><i class="far fa-clock"></i><span id="horaires_topheader">'.get_option('horaires_topheader').'</span></span>';
                    }
                    ?>
                </div>

                <?php
                the_content();

                wp_link_pages( array(
                    'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'agencepointcom' ),
                    'after'  => '</div>',
                ));
                ?>
            </div>
        </header><!-- .entry-header -->


        <?php agencepointcom_post_thumbnail(); ?>

        <div class="contact-entry-content">

<!--            <div class="picture-1-contact">-->
<!--                --><?php
//                $image = get_field('image');
//                $size = 'full'; // (thumbnail, medium, large, full or custom size)
//                if( $image ) {
//                    echo wp_get_attachment_image( $image, $size );
//                } else
//                    echo 'empty'; ?>
<!--            </div>-->
            <!--  Iframe map  -->
            <div class="map">
                <iframe src="https://snazzymaps.com/embed/338832" style="border:none;"></iframe>
            </div>

        </div><!-- .entry-content -->
    </div>
</article><!-- #post-<?php the_ID(); ?> -->


