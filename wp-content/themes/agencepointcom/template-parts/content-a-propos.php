<?php
/**
 * Template part for displaying page content in template-a-propos.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Agence_Point_Com
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<?php agencepointcom_post_thumbnail(); ?>
    <!-- Carousel -->
  <header class="a-propos-entry-header">
      <div class="container-carousel a-propos-top">
          <div class="carousel owl-carousel">
              <div class="items item-1">
                  <?php
                  $image = get_field('image3');
                  $size = 'full'; // (thumbnail, medium, large, full or custom size)
                  if( $image ) {
                      echo wp_get_attachment_image( $image, $size );
                  } else
                      echo 'empty'; ?>
              </div>
              <div class="items item-2">
                  <?php
                  $image = get_field('image3');
                  $size = 'full'; // (thumbnail, medium, large, full or custom size)
                  if( $image ) {
                      echo wp_get_attachment_image( $image, $size );
                  } else
                      echo 'empty'; ?>
              </div>
              <div class="items item-3">
                  <?php
                  $image = get_field('image3');
                  $size = 'full'; // (thumbnail, medium, large, full or custom size)
                  if( $image ) {
                      echo wp_get_attachment_image( $image, $size );
                  } else
                      echo 'empty'; ?>
              </div>
          </div>
      </div>

      <!--   Content   -->
           <?php
               the_content();
                wp_link_pages( array(
                    'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'agencepointcom' ),
                    'after'  => '</div>',
                ));
           ?>
  </header><!-- .entry-header -->
    <!--  Titre de millieu de page -->
        <div class="d-fs-20">
            <h2 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h2>
        </div>
<div class="a-propos-container-w">
<!-- Carousel -->
    <div class="container-carousel">
        <div class="carousel owl-carousel">
            <div class="items item-1">
                <?php
                $image = get_field('image3');
                $size = 'full'; // (thumbnail, medium, large, full or custom size)
                if( $image ) {
                    echo wp_get_attachment_image( $image, $size );
                    } else
                        echo 'empty'; ?>
            </div>
            <div class="items item-2">
                <?php
                $image = get_field('image3');
                $size = 'full'; // (thumbnail, medium, large, full or custom size)
                if( $image ) {
                    echo wp_get_attachment_image( $image, $size );
                } else
                    echo 'empty'; ?>
            </div>
<!--            <div class="items item-3">-->
<!--                --><?php
//                $image = get_field('image');
//                $size = 'medium'; // (thumbnail, medium, large, full or custom size)
//                if( $image ) {
//                    echo wp_get_attachment_image( $image, $size );
//                } else
//                    echo 'empty'; ?>
<!--            </div>-->
        </div>
    </div>

<!-- Notre équipe paramétrable depuis la page à propos de wordpress -->
        <div class="a-propos-equip-member">
            <h3> NOTRE ÉQUIPE </h3>
            <?php if(have_rows('equipe')):?>
                <ul class="a-propos-list-group">
                    <?php while( have_rows('equipe')): the_row(); ?>
                        <li>
                            <?php the_sub_field('nom');?>
                            <?php the_sub_field('separateur');?>
                            <?php the_sub_field('profession');?>
                        </li>
                    <?php endwhile; ?>
                </ul>
            <?php endif; ?>
        </div>
    </div>
    <!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->