<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Agence_Point_Com
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div id="conteneur_footer">
			<div class="conteneur_footer_sidebars">
				<?php if ( is_active_sidebar( 'sidebar-footer1' ) ) { ?>
					<div id="sidebar-footer1" class="sidebars-footer">
					    <?php dynamic_sidebar( 'sidebar-footer1' ); ?>
					</div>
				<?php } ?>
				<?php if ( is_active_sidebar( 'sidebar-footer2' ) ) { ?>
					<div id="sidebar-footer2" class="sidebars-footer">
					    <?php dynamic_sidebar( 'sidebar-footer2' ); ?>
					</div>
				<?php } ?>
				<?php if ( is_active_sidebar( 'sidebar-footer3' ) ) { ?>
					<div id="sidebar-footer3" class="sidebars-footer">
					    <?php dynamic_sidebar( 'sidebar-footer3' ); ?>
					</div>
				<?php } ?>
				<?php if ( is_active_sidebar( 'sidebar-footer4' ) ) { ?>
					<div id="sidebar-footer4" class="sidebars-footer">
					    <?php dynamic_sidebar( 'sidebar-footer4' ); ?>
					</div>
				<?php } ?>
				<?php if ( is_active_sidebar( 'sidebar-footer5' ) ) { ?>
					<div id="sidebar-footer5" class="sidebars-footer">
					    <?php dynamic_sidebar( 'sidebar-footer5' ); ?>
					</div>
				<?php } ?>
				
				
			</div>
		</div>
		<div class="site-info">
			<span class="copyright_footer">&copy; <?php echo date("Y"); ?> <?php echo get_bloginfo( 'name' ); ?></span>
			<span class="sep"> | </span>
				<span class="agencepointcom">SITE CRÉÉ AVEC PASSION PAR <a href="https://agencepoint.com/" target="_blank">AGENCE POINT COM PERPIGNAN</a></span>
		</div><!-- .site-info -->
		<div id="fleche_scroll" class="fleche_scroll">
            <i class="fa fa-arrow-up" aria-hidden="true"></i>
        </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>
