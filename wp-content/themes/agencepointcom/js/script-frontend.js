jQuery(document).ready( function ($){

	$("#fleche_scroll").click(function() {
         $('html, body').animate({
            scrollTop: $("body").offset().top-350
        }, 1000);
    });

    $(document).scroll(function() {
      var y = $(this).scrollTop();
      if (y > 800) {
        $('#fleche_scroll').fadeIn();
      } else {
        $('#fleche_scroll').fadeOut();
      }
    });


    $(".carousel").owlCarousel({
        loop: true,
        nav: true,
        navigationText: ["<i class=\"fas fa-angle-right\"></i>","<i class=\"fas fa-angle-left\"></i>"],
        dots:false,
        items: 1
    });

});



