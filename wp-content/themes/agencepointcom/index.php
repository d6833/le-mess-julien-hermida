<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Agence_Point_Com
 */

get_header();
?>
<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="entry-content">
				<div class="conteneur_sidebar">
					<?php
				get_sidebar();
				?>
				</div>
				<div class="conteneur_acuts">
					<?php
					if ( have_posts() ) :

						// if ( is_home() && ! is_front_page() ) :
							?>
							<header class="entry-header titre_page">
								
								<div class="conteneur_titre_sous_titre">
									<h1 class="entry-title">Nos actualités</h1>
									<div class="conteneur_sous_titre">Page d’archive</div>
								</div>
							</header>
							<?php
						// endif;

						/* Start the Loop */
						while ( have_posts() ) :
							the_post();

							/*
							 * Include the Post-Type-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
							 */
							get_template_part( 'template-parts/content', 'archive' );

						endwhile;

						

					else :

						get_template_part( 'template-parts/content', 'none' );

					endif;
					?>
				</div>
				
			
		</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php



include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if ( is_plugin_active( 'wp-pagenavi/wp-pagenavi.php' ) ) {
  wp_pagenavi();
} 




get_footer();
