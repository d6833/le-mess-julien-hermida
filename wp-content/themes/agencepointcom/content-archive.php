<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Agence_Point_Com
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('conteneur_single_archive'); ?>>
		<div class="conteneur_image_archive">
			<?php
			  				if ( has_post_thumbnail() ) {
							    the_post_thumbnail();
							}
							
			  			?>
		</div>
		<div class="conteneur_texte_archive">
			<?php
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			?>
			<div class="conteneur_date_cat_actu_archive">
	    		<span class="conteneur_date_actu_archive">
		    		<?php echo get_the_date('j F Y'); ?>
		    	</span>
		    	<span class="conteneur_cat_actu_archive">
		    		<?php echo get_the_category_list(', '); ?>
		    	</span>
				
	    	</div>
	    	<div class="conteneur_excerpt_archive">
	    		<?php echo the_excerpt(); ?>
	    	</div>
	    <div class="conteneur_bouton_lire_la_suite">
	    	<a class="boutons_lirelasuite" href="<?php echo esc_url( get_permalink()); ?>">Lire la suite</a>
	    </div>
		</div>

</article><!-- #post-<?php the_ID(); ?> -->

